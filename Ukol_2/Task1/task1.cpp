/*
* Nazev programu: Kinematika
*
* Autor: Darius Xerxes
*
* Datum: 26.11.2021
*  Tento program je pro vypocet rovnomerne zrychleneho pohybu. 
* Ve funkci intro se nastaví vstupni hodnoty, pokud jsou hodnoty zaporne program skonci. 
* Dale se ve funcki vola funkce kinematika, ktera zajistuje veskere vypocty a pote jejich vypsani, 
* vypisuje i typ pohybu. Funkce kinematika take vypisuje prehledovou tabulku.
*/

#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <stdio.h> //vstupy vystupy
#include <math.h>  //matematicke funkce
#include "task1.h"

// -- globální promìnné
char nazev_ukolu[255];
int uvod = 1;
// vstupní hodnoty
const double s0 = 0.0; // poèáteèní dráha
double v0;  // poèáteèní rychlost
double v1;  // rychlost v èase t0
double t0;  // poèáteèní èas
double t;   // koneèný èas
// vystupní hodnoty
double a;   // zrychlení
double s;   // dráha ujetá v èase <t>
double v;   // rychlost v èase <t>
double pomer;   // pomìr rychlostí v1 a v0
int typPohybu;  // identifikator typu pohybu {1 - zrychlený pohyb; 2 - konstantní pohyb; 3 - zpomalený pohyb}

void note()
// funkce zobrazí informace o autorovi a o realizaci úkolu
{//todo(1)
    if (uvod == 1) {
        uvod = 0;
        printf("Kinematika - program pro vypocet rovnomerne zrychleneho pohybu.\n\n");
        printf("jmeno: ####\n");    // todo: zobrazit jméno a pøíjmení autora
        printf("login: ######\n");    // todo: zobrazit login autora
        printf("datum plneni ukolu: 03.11.2022\n");   // todo: zobrazit datum realizace úkolu ve tvaru napø. 17.10.2022

        strcpy(nazev_ukolu, "nazev ukolu: ZSP - Domaci ukol c. 2 - \"Kinematika\"\n");

        // todo: do promìnné zapsat text s názvem úkolu (bez tìch pièatých závorek): <nazev ukolu: ZSP - Domaci ukol c. 2 - "Kinematika"> a zajistit dvakrát odøádkování
        printf(nazev_ukolu);
    }
}

int kinematika();

int intro(float v0_p, float v1_p, float t0_p, float t_p)
// funkce, ktera nahrazuje naètení hodnot z klávesnice uivatele,
// kontroluje se zde, zda jsou èísla záporná, pokud ano, pak funkce skonèí, 
// pokud ne, pak se zavolá funkce kinematika a výpis, zda probìhl program
// návratová hodnota indikuje úspìch provedení operace nebo typ chyby
{
    v0 = v0_p;
    v1 = v1_p;
    t0 = t0_p;
    t = t_p;

    if (v0 < 0) {
        printf("Vstupni hodnoty nejsou platne.Hodnoty nesmi byt zapornymi cisly.\nUspesne neprovedeno.\n");
        return -1;
    }

    if (t0 < 0) {
        printf("Vstupni hodnoty nejsou platne.Hodnoty nesmi byt zapornymi cisly.\nUspesne neprovedeno.\n");
        return -3;
    }

    if (v1 < 0) {
        printf("Vstupni hodnoty nejsou platne.Hodnoty nesmi byt zapornymi cisly.\nUspesne neprovedeno.\n");
        return -2;
    }

    if (t < 0) {
        printf("Vstupni hodnoty nejsou platne.Hodnoty nesmi byt zapornymi cisly.\nUspesne neprovedeno.\n");
        return -4;
    }

    // ------
    // 
    //todo(2): Podminky na zapornost vstupnich hodnot. 
    //      Kontrolovat smysluplnost vstupních hodnot v0, v1, t0, t a to kontrolovat v uvedeném poøadí. Hodnoty nesmí být záporné. 
    //      Funkce intro vrátí hodnotu 0, pokud jsou hodnoty v poøádku. A zavolá funkci kinematika pro realizaci výpoètu.
    //      Vrátí hodnotu -1 : patnì je v0; -2 : patnì je v1; -3 : patnì je t0; -4 : patnì je t. V pøípadì zjitìné chyby navíc zobrazí informaci
    //      <Vstupni hodnoty nejsou platne.Hodnoty nesmi byt zapornymi cisly.> a na dalí øádek <Uspesne neprovedeno.>.

    kinematika();

    return 0;
}

void kinematika_vypocet()
// funkce dopoèítá na základì vstupních hodnot vechny výstupní hodnoty
{//todo(3):
    // realizace výpoètu výstupních hodnot dle vzorcù a logického vyhodnocení

    a = (v1 - v0) / t0;
    v = v0 + (a * t);
    s = s0 + (v0 * t) + (0.5 * a) * (t * t);
    pomer = v1 / v0;

    if (a == 0) {
        typPohybu = 2;
    }

    else {
        typPohybu = a < 0 ? 1 : 3;
    }
}


void kinematika_vypisPohybu() 
// funkce pro vypis informace o pohybu
// zobrazí textový popis pohybu zjitìného typu pohybu: 
//  <Pohyb je rovnomerne zpomaleny>; <Pohyb je konstantni>; <Pohyb je rovnomerne zrychleny> a zajistí jedno odøádkování
{
    //todo(4):
    if (typPohybu == 1) {
        printf("Pohyb je rovnomerne zpomaleny\n");
    }

    else if (typPohybu == 2) {
        printf("Pohyb je konstantni\n");
    }

    else if (typPohybu == 3) {
        printf("Pohyb je rovnomerne zrychleny\n");
    }

    return;
}

void kinematika_vypisTabulky()
// funkce vypíe výsledky a pøehledovou tabulku dle vzoru uvedeném v zadání
{    
    printf("Tabulka vysledku\n");
    //todo(5):
    printf("\t\tv0\t\t %.2fkm/h\n", v0);
    printf("\t\tv1\t\t %.2fkm/h\n", v1);

    printf("\t\tt0\t\t %.2fh\n", t0);
    printf("\t\tt\t\t %.2fh\n", t);

    printf("\t\ta\t\t %.2fkm/h^2\n", a);
    printf("\t\tv\t\t %.2fkm/h\n", v);

    printf("\t\ts\t\t %.2fkm/h\n", s);
    printf("\t\tpomer v\t\t %.2f%\n", pomer);

    return;
}



int kinematika()  // funkce kinematika
{
    note();

    //todo(5):
    // zajistit realizaci výpoètu a zobrazení výsledkù podmínit validitou vstupních dat. 
    kinematika_vypocet();

    kinematika_vypisPohybu();
    kinematika_vypisTabulky();
    return 1;
}



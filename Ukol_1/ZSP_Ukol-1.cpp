#include <iostream>
#define DPH 20
#define POCET_ZNAMEK 5

//Kvuli prehlednosti preferuji deklaraci funkci pred mainem a samotnou funkci pod mainem
void u1_1();
void u1_2();
void u1_3();

int main() {
    //u1_1();
    u1_2();
    //u1_3();
}

void u1_1() {
    int pocetKusu, cenaKusu;

    //Nacteni poctu kusu
    printf("Zadejte pocet kusu: ");
    int jeCeleCislo = scanf("%d", &pocetKusu);
    //Overeni hodnoty vstupu
    if(jeCeleCislo != 1 || pocetKusu <= 0) {
        fprintf(stderr, "Neplatne vstupni hodnoty, pouze cela cisla v rozsahu [1,2147483647]");
        return;
    }

    //Nacteni ceny za kus
    printf("Zadejte cenu za kus (bez DPH): ");
    jeCeleCislo = scanf("%d", &cenaKusu);
    //Overeni hodnoty vstupu
    if(jeCeleCislo != 1 || cenaKusu <= 0) {
        fprintf(stderr, "Neplatne vstupni hodnoty, pouze cela cisla v rozsahu [1,2147483647]");
        return;
    }

    //Vypocet hodnoty DPH
    int cenaSDph = ((float)cenaKusu / 100) * (100 + DPH);

    //Vypis hodnot
    printf("Uctenka\n");
    printf("Cena bez DPH/ks %d Kc \t Cena s DPH/ks %d Kc\n", cenaKusu, cenaSDph);
    printf("Pocet kusu: %d \t Cena bez DPH %d \t Cena s DPH (%d %) %d Kc \n", pocetKusu, (pocetKusu * cenaKusu), DPH, (pocetKusu * cenaSDph));
}

void u1_2() {
    //Jako konstanu jsem pouzil pocet znamek
    int znamky[POCET_ZNAMEK], i = 0;
    while (i < POCET_ZNAMEK) {
        int znamka;
        printf("Zadejte %d. znamku: ", (i + 1));
        int jeCislo = scanf(" %d", &znamka);
        //Kontrola jestli se vstupni hodnota cislo a jestli splnuje rozsah
        if(jeCislo == 1 && znamka > 0 && znamka <= 5) {
            //Pokud je vse v poradku tak uloz do pole znamek
            znamky[i] = znamka;
            //Pokracuj na dalsi znamku
            i++;
        }

        else {
            printf("Neplatna hodnota znamky, povolene hodnoty jsou [1,5]\n");
        }
    }

    printf("\n----------------------------------\n");

    float prumer = 0;
    bool vyznamenani = true, prospel = true;
    printf("Znamky: ");
    for (int j = 0; j < POCET_ZNAMEK; j++) {
        //Vypis hodnot znamek
        printf("%d\t", znamky[j]);
        //Secte znamky v poli
        prumer += znamky[j];
        //Pokud je znamka horsi nez 2, zak neni hodnocen vyznamenanim
        znamky[j] > 2 ? vyznamenani = false : 0;
        //Pokud je znamka, zak neprospel
        znamky[j] == 5 ? prospel = false : 0;
    }


    //Vypocet prumeru ze souctu znamek
    prumer /= (float)POCET_ZNAMEK;
    //Pokud ma zak horsi prumer nez 1.5 neni hodnocen vyznamenanim
    if(prumer > 1.5) {
        vyznamenani = false;
    }

    printf("\nPrumerna znamka: %.2f", prumer);
    printf("\nProspel s vyznamenanim: %s", vyznamenani ? "Ano" : "Ne");
    printf("\nProspel: %s", prospel ? "Ano" : "Ne");
    printf("\nNeprospel: %s", prospel ? "Ne" : "Ano");
    printf("\n");
}

void u1_3() {
    //Zkratky men maji 3 znaky
    char mena[3];
    float hodnotaMeny;
    int pocetMeny;
    //Nacte nazev meny
    printf("Zadejte nazev meny: ");
    scanf("%3s", &mena);
    //Nacte hodnotu meny
    printf("Zadejte hodnotu meny: ");
    int jeCislo = scanf("%f", &hodnotaMeny);
    if(jeCislo != 1 || jeCislo == EOF) {
        fprintf(stderr, "Neplatna hodnota, povolena jsou pouze cisla\n");
        return;
    }

    //Nacte kolik meny chce uzivatel nakoupit
    printf("Zadejte kolik meny chcete nakoupit: ");
    int jeCeleCislo = scanf("%d", &pocetMeny);
    if(jeCeleCislo != 1) {
        fprintf(stderr, "Neplatna hodnota, povolena jsou pouze cela cisla\n");
        return;
    }

    //Vypis hodnot
    printf("1 %s = %.1f Kc\n", &mena, hodnotaMeny);
    printf("Nakup: %d %s\n", pocetMeny, &mena);
    float celkem = (float)pocetMeny * hodnotaMeny;
    printf("Celkem: %d x %.1f = %.1f Kc Zaokrouhleno: %.0f Kc\n", pocetMeny, hodnotaMeny, celkem, celkem);
}
#include "pch.h"
#include "p1.h"
#include <iostream>

int test_id_dne(int id_dne) {
	// otestuje (ov���) identifik�tor dne v t�dnu, p�edpokl�d� se hodnota identifik�toru v rozsahu <1; 7>
	// vr�t� hodnotu 0 v p��pad� chybn�ho identifik�toru, jinak hodnotu 1
	return (int)id_dne > 0 && id_dne <= POCET_TEPLOT_TYDEN;
}


char* nazev_dne(int id_dne) {
	// vr�t� n�zev dne podle identifik�toru
	char nazvyDnu[10][10] = {
		"Pondeli",
		"Utery",
		"Streda",
		"Ctvrtek",
		"Patek",
		"Sobota",
		"Nedele"
	};
	char* s;

	if (test_id_dne(id_dne)) {
		s = (char*) malloc(10 * sizeof(char));
		if (s == NULL)
			return NULL;  // chyba: nepoda�ila se alokace �et�zce v pam�ti

		strcpy_s(s, 10, nazvyDnu[id_dne - 1]); // kopie n�zvu dne odpov�daj�c� dnu ur�en�m jeho identifik�tor v t�dnu
		return s;
	}
	else {
		return NULL;
	}
}


int zapis_teplotu(double *teploty_tyden, int pocet_teplot, int id_dne, double teplota) {
	// zap�e do tabulky teplot teplotu pro zadan� den, pozice v tabulce pro odpovidaj�c� den je ur�ena hodnotou id_dne
	// p�edpokl�d� se hodnota identifik�toru dne ��slov�no od 1, p�i chybn�ch datech, p�i neplatn�m identifik�toru dne vr�t� hodnotu 0, jinak hodnotu 1

	if (test_id_dne(id_dne) == 1) {
		teploty_tyden[id_dne - 1] = teplota;
		return 1;
	}

	return 0; // todo du done, vyu��t funkce test_id_dne
}


int precti_teplotu(double *teploty_tyden, int pocet_teplot, int id_dne, double *teplota) {
	// p�e�te z tabulky teplot teplotu pro zadan� den, pozice v tabulce pro odpovidaj�c� den je ur�ena hodnotou id_dne, hodnota teploty je zaps�na do v�stupn�ho parametru teplota
	// p�edpokl�d� se hodnota identifik�toru dne ��slov�no od 1, p�i chybn�ch datech, p�i neplatn�m identifik�toru dne vr�t� hodnotu 0, jinak hodnotu 1

	if (test_id_dne(id_dne) == 1) {
		*teplota = teploty_tyden[id_dne - 1];
		return 1;
	}

	return 0; // todo du done, vyu��t funkce test_id_dne
}


double prumerna_teplota(double* teploty_tyden, int pocet_teplot) {
	// spo��t� pr�m�rnou teplotu ze v�ech dn� v t�dnu, v�sledek vr�t� n�vratovou hodnotou funkce
	double soucetTeplot = 0;

	for (int i = 0; i < pocet_teplot; i++) {
		soucetTeplot += teploty_tyden[i];
	}

	return (double)soucetTeplot / (double)pocet_teplot;

	//return 0; // todo du done
}


double maximalni_teplota(double* teploty_tyden, int pocet_teplot) {
	// ur�� maxim�ln� teplotu ze v�ech dn� v t�dnu, v�sledek vr�t� n�vratovou hodnotou funkce
	double maximalniTeplota = teploty_tyden[0];

	for (int i = 0; i < pocet_teplot; i++) {
		if (maximalniTeplota < teploty_tyden[i]) {
			maximalniTeplota = teploty_tyden[i];
		}
	}

	return maximalniTeplota;
}


int pozice_teploty(double* teploty_tyden, int pocet_teplot, double teplota) {
	// vr�t� identifk�tor dne (pozici) s hledanou teplotou, ��slov�no od nuly
	for (int i = 1; i <= pocet_teplot; i++){
		double teplotaDen;
		int kontrola = precti_teplotu(teploty_tyden, pocet_teplot, i, &teplotaDen);
		if (kontrola == 1) {
			if (teplotaDen == teplota) {
				return i;
			}
		}
	}

	return 0; //V zadani neni uvedeno jakou hodnotu vracet v pripade nenalezeni hodnoty
}


int pocet_dnu_teplota(double* teploty_tyden, int pocet_teplot, double teplota) {
	// spo�te po�et dn�, ve kter�ch nastala zadan� teplota
	int pocet = 0;

	for (int i = 0; i < pocet_teplot; i++) {
		if (teploty_tyden[i] == teplota) {
			pocet++;
		}
	}

	return pocet;
}


void nacti_teplotu_den(double *teploty_tyden, int pocet_teplot) {
	// p�e�te do tabulky teplot hodnotu pro zadan� den, zajisti na�ten� identifk�toru dne v rozsahu 1 a� 7 a teploty ze standardniho vstupu, zajisti zapis do tabulky teplot
	printf("Zadej teplotu pro konkretni den ve formatu <den mezera teplota>: ");
	// todo du done
	// pou��t jeden p��kaz scanf pro na�ten� v�ech �daj�
	// + vyu��t funkce zapis_teplotu
	int den, kontrola;
	double teplota;
	kontrola = scanf_s("%d %lf", &den, &teplota);

	if (test_id_dne(den) == 1 && kontrola == 2) {
		printf("%lf", teplota);
		zapis_teplotu(teploty_tyden, pocet_teplot, den, teplota);
	}

	else {
		printf("Neplatne vstupni hodnoty\n");
	}

}

void nacti_teplotu_tyden(double *teploty_tyden, int pocet_teplot) {
	// na�te do tabulky teplot data za cel� t�den
	printf("Zadani teplot pro cely tyden\n");
	// todo du done
	// + vyu��t funkce zapis_teplotu
	int i = 1;

	while (i <= pocet_teplot) {
		double teplota;
		char vstup[50] = "\0", * chyba;
		printf("Zadejte teplotu pro den %s: ", nazev_dne(i));
		//Pouzil jsem fgets, protoze narozdil od scanf umoznuje nacist prazdny vstup od uzivatele
		fgets(vstup, 50, stdin);
		//Pokud uzivatel zada jiny znak nez cislo, ulozi ho do promenne chyba
		teplota = strtod(vstup, &chyba);
		

		//Pokud uzivatel nezadal cislo nebo je vstup prazdny, tak ohlasi chybu a opakuje zadani pro stejny den
		if (*chyba == '\n' && strlen(vstup) > 1) {
			zapis_teplotu(teploty_tyden, pocet_teplot, i, teplota);
			i++;
		}

		else {
			printf("Neplatna hodnota! \n");
		}
	}
}

void tisk_teploty(double *teploty_tyden, int pocet_teplot) {
	// vyp�e obsah tabulky teplot
	// todo du done
	// + vyu��t funkce precti_teplotu

	printf("\nTydenni prehled teplot:\n");

	for (int i = 1; i <= pocet_teplot; i++) {
		double teplota;
		precti_teplotu(teploty_tyden, pocet_teplot, i, &teplota);
		printf("%s\t\t%.4f \n", nazev_dne(i), teplota);
	}
}

void tisk_statistika(double *teploty_tyden, int pocet_teplot) {
	// z�sk� hodnoty pro statistku (pr�m�rn� teplota, maxim�ln� teplota, po�et dn� s maxim�ln� teplotou) a vytiskne v�sledky na standardn� v�stup
	// todo du done

	printf("[");

	for (int i = 1; i <= pocet_teplot; i++) {
		double teplota;
		if (precti_teplotu(teploty_tyden, pocet_teplot, i, &teplota) == 1) {
			if (i == pocet_teplot) {
				printf("%d: %.4f", i, teplota);
			}

			else {
				printf("%d: %.4f, ", i, teplota);
			}		
		}
	}

	printf("] \n");

	printf("Prumerna teplota tydne je %.4f stupnu\n", prumerna_teplota(teploty_tyden, pocet_teplot));
	double maxTeplota = maximalni_teplota(teploty_tyden, pocet_teplot);
	int maxTeplotaDen = pozice_teploty(teploty_tyden, pocet_teplot, maxTeplota);
	printf("Maximalni teplota tydne je %.4f stupnu\n", maxTeplota);
	printf("Pocet dnu s maximalni teplotou: %d (%s)\n", pocet_dnu_teplota(teploty_tyden, pocet_teplot, maxTeplota), nazev_dne(maxTeplotaDen));
}

void zapis_do_souboru_statistika(double* teploty_tyden, int pocet_teplot, char* nazev_souboru) {
	// z�sk� hodnoty pro statistiku (pr�m�rn� teplota, maxim�ln� teplota, po�et dn� s maxim�ln� teplotou) a zap�e v�sledky do textov�ho souboru
	// todo du done

	FILE* fp;
	errno_t error;
	error = fopen_s(&fp, nazev_souboru, "w");

	if (error != 0) {
		printf("Soubor se nepodarilo otevrit!");
		return;
	}

	fprintf(fp, "[");

	for (int i = 1; i <= pocet_teplot; i++) {
		double teplota;
		if (precti_teplotu(teploty_tyden, pocet_teplot, i, &teplota) == 1) {
			if (i == pocet_teplot) {
				fprintf(fp, "%d: %.4f", i, teplota);
			}

			else {
				fprintf(fp, "%d: %.4f, ", i, teplota);
			}
		}
	}

	fprintf(fp, "] \n");

	fprintf(fp, "Prumerna teplota tydne je %.4f stupnu\n", prumerna_teplota(teploty_tyden, pocet_teplot));
	double maxTeplota = maximalni_teplota(teploty_tyden, pocet_teplot);
	int maxTeplotaDen = pozice_teploty(teploty_tyden, pocet_teplot, maxTeplota);
	fprintf(fp, "Maximalni teplota tydne je %.4f stupnu\n", maxTeplota);
	fprintf(fp ,"Pocet dnu s maximalni teplotou: %d (%s)\n", pocet_dnu_teplota(teploty_tyden, pocet_teplot, maxTeplota), nazev_dne(maxTeplotaDen));

	fclose(fp);
}

void zapis_do_souboru_teploty(double *teploty_tyden, int pocet_teplot, char *nazev_souboru) {
	// zapise tabulku teplot do souboru
	// todo du done

	FILE* fp;
	errno_t error;
	error = fopen_s(&fp, nazev_souboru, "w");

	if (error != 0) {
		printf("Soubor se nepodarilo otevrit!");
		return;
	}

	for (int i = 0; i < pocet_teplot; i++) {
		fprintf(fp, "%.4f\n", teploty_tyden[i]);
	}

	printf("Data byla zapsana do souboru %s", nazev_souboru);
	
	fclose(fp);
}


void nacti_ze_souboru_teploty(double *teploty_tyden, int pocet_teplot, char *nazev_souboru) {
	// nacte tabulku teplot ze souboru
	// todo du done

	FILE* fp;
	errno_t error;

	error = fopen_s(&fp, nazev_souboru, "r");

	if (error != 0) {
		printf("Soubor se nepodarilo otevrit!");
		return;
	}

	double teplota;
	int i = 1;

	while (fscanf_s(fp, "%lf", &teplota) != EOF && i <= pocet_teplot) {
		zapis_teplotu(teploty_tyden, pocet_teplot, i, teplota);
		i++;
	}

	printf("Soubor byl uspesne nacten");

	fclose(fp);
}

// ------------------------

void menu() {
	printf("\n\n| Menu\n");
	printf("| 0 - KONEC\n");
	printf("| 1 - Zadat teploty na klavesnici\n");
	printf("| 2 - Nacist teploty ze souboru\n");
	printf("| 3 - Ulozit teploty do souboru\n");
	printf("| 4 - Zobrazit teploty\n");
	printf("| 5 - Zobrazit teplotu v zadany den\n");
	printf("| 6 - Zmena teploty v zadany den\n");
	printf("| 7 - Statistika teplot\n");
}


char volba_menu() {
	printf("\nZadej cislo volby: ");
	char volba = getchar();
	while (getchar() != '\n');
	printf("\n");
	return volba;
}


void operace(double *teploty_tyden) {
	int konec = 0;
	while (!konec) {
		menu();

		char volba = volba_menu();

		switch (volba) {
			case '0': {
				konec = 1;
			} break;
			case '1': {
				nacti_teplotu_tyden(teploty_tyden, POCET_TEPLOT_TYDEN);			
			} break;
			case '2': {
				//char soubor[10] = "data.txt";
				// todo du done - na��st n�zev souboru
				char soubor[20];
				printf("Zadejte nazev souboru (max. delka je 20 znaku): ");
				scanf_s("%s", &soubor, 20);

				nacti_ze_souboru_teploty(teploty_tyden, POCET_TEPLOT_TYDEN, soubor);			
			} break;
			case '3': {
				//char soubor[10] = "data.txt";
				// todo du done - na��st n�zev souboru

				char soubor[20];
				printf("Zadejte nazev souboru (max. delka je 20 znaku): ");
				scanf_s("%s", &soubor, 20);

				zapis_do_souboru_teploty(teploty_tyden, POCET_TEPLOT_TYDEN, soubor);			
			} break;
			case '4': {
				tisk_teploty(teploty_tyden, POCET_TEPLOT_TYDEN);
			} break;
			case '5': {
				char vstup[50] = "\0", *chyba;
				printf("Zadejte index dne: ");
				fgets(vstup, 50, stdin);
				int den = strtol(vstup, &chyba, 10);

				if (*chyba == '\n' && strlen(vstup) > 1) {
					if (test_id_dne(den) == 1) {
						double teplota;
						precti_teplotu(teploty_tyden, POCET_TEPLOT_TYDEN, den, &teplota);
						printf("Teplota v tento den: %.4f \n", teplota);
					}

					else {
						printf("Neplatna hodnota! \n");
					}
				}

				else {
					printf("Neplatna hodnota! \n");
				}

			} break;
			case '6': {

				//Nacteni dne jsem se rozhodl resit pomoci pripravene funkce
				nacti_teplotu_den(teploty_tyden, POCET_TEPLOT_TYDEN);

				//// p�e�ti den a hodnotu teploty
				//int den = 0;
				//double teplota = 0;
				//// todo du - na��st identifik�tor dne a teplotu
				//zapis_teplotu(teploty_tyden, POCET_TEPLOT_TYDEN, den, teplota);
			} break;
			case '7': {
				char zapsat;
				tisk_statistika(teploty_tyden, POCET_TEPLOT_TYDEN);

				int uspech = 0;

				while (uspech == 0) {
					printf("Chcete ulozit hodnoty do souboru? [a/n]: ");
					scanf_s(" %c", &zapsat, 1);

					if (zapsat == 'a' || zapsat == 'A') {
						char soubor[20];
						printf("Zadejte nazev souboru (max. delka je 20 znaku): ");
						scanf_s("%s", &soubor, 20);

						zapis_do_souboru_statistika(teploty_tyden, POCET_TEPLOT_TYDEN, soubor);

						printf("Data byla zapsana do souboru %s \n", soubor);

						uspech = 1;
					}

					if (zapsat == 'n' || zapsat == 'N') {
						uspech = 1;
					}

					else {
						printf("Byla zadana neplatna hodnota!");
					}
				}

				// Nen� v uk�zkov�m exe-souboru. (!!!)
				// Dotaz�t se na z�pis do souboru (ulo�en� do souboru), odpov�� stiskem kl�vesy <A> nebo <N>. V p��pad� odpov�di ano, na��st n�zev souboru. A zobrazit informaci, "Data byla zaps�na do souboru nazev souboru."
			} break;
			default: {
				printf("Neplatna hodnota volby!");
			} break;
		}
	}
}

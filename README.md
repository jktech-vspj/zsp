# Základy strukturovaného programování

### Popis
Každé cvičení obsahuje zadání a vypracovaný úkol do předmětu základy strukturovaného programování. Pro druhé a třetí cvičení je potřeba použít Visual Studio. 

### Stažení
```git
git clone git@gitlab.com:jktech-vspj/zsp.git
```

### Požadavky
- [Visual Studio](https://visualstudio.microsoft.com/)